## RULE 1/ classification factor or integer or character, will be converted to factors
## RULE 2/ environment can contain numeric values only. Numeric values will be normalized. Categorial/factor variables have to be converted using one-hot encoding 
train <- function(releves, classification, environment=NULL, validation_split=0.8, type="nn"){
    classification <- as.factor(classification)
    if("numeric" %in% class(environment)) environment <- matrix(environment,nrow(releves),1)
    nbclass <- length(levels(classification))
    if(type=="nn"){
        ## automatic rescaling of releves abundance (uneffective if 0/1 presence/absence)    
        if(is.null(environment)){
            datanormd <- maxscale(releves,1)
        } else{
            warning("factor variables must have been transformed with one-hot encoding")
            datanormd <- cbind(maxscale(releves,1), minmaxscale(environment,2))
        } 
    }
    if(type=="rf"){
        if(is.null(environment)){
            datanonorm <- releves
        } else{
            datanonorm <- cbind(releves, environment)
        }
    }
    ## k-fold cross validation
    k <- as.integer(1/(1-validation_split))
    folds <- create_folds(classification, k=k, type="stratified")
    pred <- matrix(NA, nrow(releves), nbclass)
    if(type=="nn") kweights <- list()
    if(type=="rf") krf <- list()    
    ifold <- 0
    for(fold in folds){
        ifold <- ifold+1
        ## train/val indices
        fold <- sample(fold)
        intrain = inval = rep(FALSE, length(classification))
        intrain[fold] <- TRUE
        inval[c(1:length(classification))[-fold]] <- TRUE
        ## if a label is absent from train, duplicate once from validation
        train_size <- table(classification[intrain])
        for(absent_label in names(which(train_size==0))){
            i <- which(classification==absent_label & inval)[1]
            # inval[i] <- FALSE
            intrain[i] <- TRUE
        }
        ## if a label is absent from validation, duplicate once from train
        val_size <- table(classification[inval])
        for(absent_label in names(which(val_size==0))){
            i <- which(classification==absent_label & intrain)[1]
            # intrain[i] <- FALSE
            inval[i] <- TRUE
        }
        
        idxtrain <- which(intrain==TRUE)
        #idxtrain <- botania.upsample(idxtrain, classification, 50)
        idxval <- which(inval==TRUE)
        #print(table(classification[idxtrain]))
        #print(table(classification[idxval]))
        if(type=="nn"){
            ## neural net
            res <- botania.trainNN(datanormd, classification, idxtrain, idxval, units=max(100,nbclass*2.5))
            pred[idxval,] <- res$predval
            kweights[[ifold]] <- res$weights
        }
        if(type=="rf"){
            ## random forest
            res <- botania.trainRF(datanonorm, classification, idxtrain, idxval, num.threads=1)
            pred[idxval,] <- res$predval
            krf[[ifold]] <- res$rf
        }
        
    }
    colnames(pred) <- levels(classification)
    rownames(pred) <- rownames(releves)
    if(type=="nn"){
        if(is.null(environment))
           variablesrange = NULL
        else
            variablesrange = apply(environment,2,function(v) c(max(v),min(v)))
        classifier <- list(type="nn", labels=levels(classification),
                           species=colnames(releves),
                           variables=colnames(environment),
                           variablesrange=variablesrange,
                           pred=pred, kweights=kweights)
    }
    if(type=="rf")
        classifier <- list(type="rf", labels=levels(classification),
                           species=colnames(releves),
                           variables=colnames(environment),
                           pred=pred, krf=krf)        
    class(classifier) <- "botania"
    return(classifier)
}

minmaxscale <- function(m,MARGIN){
    mscaled <- apply(m,MARGIN,function(v){
        if((max(v)-min(v))>0)
            v <-(v-min(v))/(max(v)-min(v))
        v
    }) 
    if(MARGIN==1) return(t(mscaled)) else return(mscaled)       
}

maxscale <- function(m,MARGIN){
    mscaled <- apply(m,MARGIN,function(v){
        if((max(v))>0)
            v <- v/max(v)
        v
    })
    if(MARGIN==1) return(t(mscaled)) else return(mscaled)
}

botania.trainNN <- function(datanormd, classification, idxtrain, idxval, units=500, epochs=20){
    nbclass <- length(levels(classification))
    iclassification <- as.integer(classification)-1L
    model <- keras_model_sequential() %>%
        layer_dense(units, input_shape = c(ncol(datanormd))) %>%
        ## https://www.tensorflow.org/tutorials/keras/overfit_and_underfit
        ## kernel_regularizer =regularizer_l2(l=0.0001)
        layer_activation("relu") %>%
        layer_dropout(0.2) %>%        
        layer_dense(nbclass) %>% 
        layer_activation("softmax")
    metric_top_5_categorical_accuracy <-
        custom_metric("top_5_categorical_accuracy", function(y_true, y_pred) {
            metric_top_k_categorical_accuracy(y_true, y_pred, k = 5)
        })
    model %>% compile(loss = "categorical_crossentropy",
                      optimizer ='adam',
                      metrics = c("accuracy",metric_top_5_categorical_accuracy)
                      )
    batch <- 32
    hist <- model %>%
        fit(x=as.matrix(datanormd[idxtrain,]),
            y=to_categorical(iclassification[idxtrain],num_classes=nbclass),
            batch_size = batch, 
            epochs = epochs,       
            shuffle=TRUE, verbose=2,                    
            validation_data = list(as.matrix(datanormd[idxval,]),to_categorical(iclassification[idxval],num_classes=nbclass)),
            callbacks = list(
                callback_early_stopping(
                    monitor = "val_accuracy",
                    min_delta = 0,
                    patience = 3,
                    verbose = 1,
                    mode = c("max"),
                    restore_best_weights = TRUE
                ))            
            )
    ## Getting the predictions probabilities
    predval <- model %>% predict(as.matrix(datanormd[idxval,]))
    ## Return
    return(list(predval=predval,weights=get_weights(model)))
}

# You have to code the characters as factors to consider them unordered 
# Internally we check for is.factor & !is.ordered
botania.trainRF <- function(datanonorm, classification, idxtrain, idxval, num.threads = 8){
    traindf <- data.frame(classification=classification[idxtrain], datanonorm[idxtrain,])
    rf <- ranger(formula=classification~., data=traindf, probability=T, num.threads=num.threads)
    ## Getting the predictions probabilities
    predval <- predict(rf, data.frame(classification=classification[idxval], datanonorm[idxval,]))$predictions
    return(list(predval=predval,rf=rf))
}

accuracy <- function(object, ...) UseMethod("accuracy") # to declare a new generic method

accuracy.botania <- function(object, classification, k=1){
    classification <- as.factor(classification)
    acc.table <- data.frame(accuracy=rep(0.,1+nlevels(classification)), row.names=c("all",levels(classification)))
    for (label in c("all",levels(classification))){
        if (label=="all"){
            idxlabel <- 1:length(classification)
        } else{
            idxlabel <- which(classification == label)
        }
        if(k==1){
            if(length(idxlabel)==1){
                acc <- as.integer(classification[idxlabel]==which.max(object$pred[idxlabel,]))
            } else{
                acc <- sum(as.integer(classification[idxlabel])==apply(object$pred[idxlabel,],1,which.max)) / length(classification[idxlabel])
            }
        } else{
            if(length(idxlabel)==1){
                acc <- as.integer(as.integer(classification[idxlabel]) %in% order(object$pred[idxlabel,],decreasing=T)[1:k])
            } else{
                acc <- sum(apply(cbind(as.integer(classification[idxlabel]),t(apply(object$pred[idxlabel,],1,order,decreasing=T)[1:k,])), 1, function(v){
                    v[1] %in% v[2:(k+1)]
                })) / length(classification[idxlabel])
            }
        }
        acc.table[label,"accuracy"] <- acc
    }
    return(acc.table)
}

botania.upsample <- function(idx, classification, minperclass=50){
    idxup <- c()
    sizes <- table(classification[idx])
    for (label in levels(classification[idx])){
        idxlabel <- which(classification[idx]==label)
        if(length(idxlabel)<minperclass){
            if(length(idxlabel)==1){
                idxup <- c(idxup, rep(idxlabel, minperclass-length(idxlabel)))
            } else{
                idxup <- c(idxup, sample(c(idxlabel), minperclass-length(idxlabel), replace=TRUE))
            }
        }
    }
    return(c(idx,idx[idxup]))
}
