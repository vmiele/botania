#  BOTANIA

<img src="img/flowers.png" width="33%"/>
<img src="img/logoINEE.png" width="33%"/>
<img src="img/logo-cbna.jpg" width="12%"/>

---

# When botanics meets AI (well, "IA" in french)

Collaborative project involving [Vincent Miele](https://lbbe-web.univ-lyon1.fr/fr/annuaires-des-membres/miele-vincent), [Sylvain Abdulhak](https://www.researchgate.net/profile/Sylvain-Abdulhak), Giuseppe Capizzi, [Stéphane Dray](https://lbbe.univ-lyon1.fr/en/annuaires-des-membres/dray-stephane) and [Wilfried Thuiller](http://www.will.chez-alice.fr/).



# 1. INSTALLATION

---


### WARNING
You need the `R` software! More info about installing `R` on your machine (Windows, Mac, Linux) is available at [CRAN](https://cran.r-project.org/)

*But* you also need the `Python` programming language. Later here, you will have to choose among 3 options  (and we can't tell what is the best option in your case, "it depends"...): 
1. Install `Python`, using Anaconda (particularily under Windows)
2. Install `Python` directly
3. Or alternatively use `R` to install `Python` (see below). 

### STEP 1 - INSTALLING R PACKAGES INSIDE R
Now you need the required `R` packages : `remotes`, `tensorflow`, `keras3`, `splitTools`, `ranger`,  `data.table`, `dplyr`. 

First, install the `remotes` library.

Then, install `tensorflow` as [explained here](https://tensorflow.rstudio.com/install/). You possibly have to install `Python` as well, with the `install_python()` command.

Then, install `keras3`  as [explained here](https://keras3.posit.co/articles/getting_started.html).

Finally, install the pure R packages `splitTools`, `ranger`,  `data.table`, `dplyr` the usual way.


### STEP 2 - INSTALLING BOTANIA INSIDE R
Restart `R`. There you are, you can install `botania`:
```
library(remotes) # or library(devtools)
install_git("https://gitlab.com/vmiele/botania.git")
```

---

# 2. GETTING STARTED 

---

You can **learn how to use botania** by reading these notebooks:

- [a classical pipeline to treat existing releves](https://gitlab.com/vmiele/botania/-/blob/main/notebooks/demoExample.ipynb?ref_type=heads)

- [another less classical pipeline, where releves are built from raw species observations](https://gitlab.com/vmiele/botania/-/blob/main/notebooks/demoReadReleves.ipynb?ref_type=heads)

---

# 3. CONTACT

---

For any question, bug or feedback, feel free to send an email to [Vincent Miele](https://lbbe.univ-lyon1.fr/-Miele-Vincent-.html) <!--or use the Gitlab Service Desk-->

---
:q

